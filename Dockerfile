ARG CENTOS_VERSION
FROM centos:${CENTOS_VERSION}
LABEL maintainer="Daniel von Essen"
ENV container=docker

# Install systemd -- See https://hub.docker.com/_/centos/
RUN yum -y update && \
  yum -y install python3 sudo && \
  yum clean all

# Create `ansible` user with sudo permissions
RUN groupadd -r ansible \
  && useradd -m -g ansible ansible \
  && echo "%ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible
